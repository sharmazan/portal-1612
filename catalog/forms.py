from django import forms
from django.contrib.auth.models import User
from .models import Company

class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        exclude = ['owner']

class NewUserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput())
    class Meta:
        model = User
        fields = ['username', 'email', 'password']
