from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Owner(models.Model):
    name = models.CharField(max_length=100)
    phone = models.CharField(max_length=100, null=True)
    user = models.OneToOneField(User)

    def __str__(self):
        return self.name


class Company(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    owner = models.ForeignKey('Owner', null=True)
    # slug = models.SlugField(max_length=100)

    def __str__(self):
        return self.title
