from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.urls import reverse

from .models import Company, Owner
from .forms import CompanyForm, NewUserForm


# Create your views here.

def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/')


def owner_create(request):
    # print(request.user)
    next_page = request.GET.get('next', '/')
    if request.method == 'POST':
        form = NewUserForm(request.POST)
        if form.is_valid():
            user = form.save()
            login(request, user)
            owner = Owner(name='test2', phone='1234567', user=user)
            owner.save()
            return redirect(next_page)
    else:
        form = NewUserForm()
    return render(request, 'catalog/owner_new.html', {'form': form})


def companies_list(request):
    # print(request.user)
    # message = 'Portal homepage'
    # if request.user:
    #     message = 'Hi, '+request.user.username
    # html = '<html><body>{}</body></html>'.format(message)
    companies = Company.objects.all()
    # print(companies)
    return render(request, 'catalog/companies_list.html', {'companies': companies})


def company_detail(request, company_id):
    # print(type(company_id))
    c = get_object_or_404(Company, id=company_id)
    owner = c.owner
    if owner:
        owners_companies = owner.company_set.all()
        owners_companies = Company.objects.filter(owner=owner)
    # print(owner.name)
    # print(owner.company_set.all())
    return render(request, 'catalog/company_detail.html', {'company': c, 'owners_companies': owners_companies})


def company_create(request):
    print(request.user)
    if request.user.is_authenticated:
        if request.user.owner:
            owner = request.user.owner
        else:
            print('User has no owner!')
        if request.method == 'POST':
            form = CompanyForm(request.POST)
            if form.is_valid():
                # user = request.user
                # print(user, user.id)
                # try:
                #     owner = Owner.objects.get(user=user)
                # except:
                #     print(Owner.objects.all())
                #     print(1/0)
                c = form.save(commit=False)
                c.owner = request.user.owner
                c.save()
                return redirect('company_detail', company_id=c.id)
        else:
            form = CompanyForm()
        return render(request, 'catalog/company_new.html', {'form': form})
    else:
        return HttpResponseRedirect(reverse('owner_create')+'?next='+reverse(company_create))


@login_required
def company_edit(request, company_id):
    company = get_object_or_404(Company, id=company_id)
    owner = company.owner
    if owner.user == request.user:
        if request.method == 'POST':
            form = CompanyForm(request.POST, instance=company)
            if form.is_valid():
                c = form.save(commit=False)
                c.owner = owner
                c.save()
                return redirect('company_detail', company_id=company.id)
        else:
            form = CompanyForm(instance=company)
        return render(request, 'catalog/company_edit.html', {'form': form, 'company': company})
    else:
        return HttpResponse('No permissions!')
