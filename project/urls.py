"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin

from catalog import views
from pages import views as pages_views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^$', pages_views.homepage, name='homepage'),
    url(r'^logout/$', views.logout_view, name='logout'),
    url(r'^catalog/owners/new/$', views.owner_create, name='owner_create'),
    url(r'^catalog/$', views.companies_list, name='companies_list'),
    url(r'^catalog/companies/(?P<company_id>[0-9]+)/$', views.company_detail, name='company_detail'),
    url(r'^catalog/companies/new/$', views.company_create, name='company_create'),
    url(r'^catalog/companies/(?P<company_id>[0-9]+)/edit/$', views.company_edit, name='company_edit'),
    url(r'^contact/$', pages_views.contact, name='contact'),
    url(r'^(?P<page_name>.+)/$', pages_views.page, name='page'),
]
